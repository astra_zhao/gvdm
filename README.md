### 技术选型

####前端
- vuejs2.0
- vue-router
- element-ui
- mock
- axios


####后端
- 核心框架：Spring Boot
- 数据库连接池：Druid
- 持久层框架：MyBatis-plus

### 系统部署

1.搭建服务端
导入https://gitee.com/chenyp/gvdm.git 代码到idea中
导入数据库文件（数据库文件在附件中）

2.搭建前端
下载svn://gitee.com/chenyp/Vue-admin 到本地
仅此此文件夹中，使用如下命令（前提是安装好npm环境）
`npm install`

`npm run dev`


### 系统演示
[![http://140.143.132.167/vue/1.jpg](http://140.143.132.167/vue/1.jpg "http://140.143.132.167/vue/1.jpg")](http://140.143.132.167/vue/1.jpg "http://140.143.132.167/vue/1.jpg")

[![http://140.143.132.167/vue/2.jpg](http://140.143.132.167/vue/2.jpg "http://140.143.132.167/vue/2.jpg")](http://140.143.132.167/vue/2.jpg "http://140.143.132.167/vue/2.jpg")
[![http://140.143.132.167/vue/3.jpg](http://140.143.132.167/vue/3.jpg "http://140.143.132.167/vue/3.jpg")](http://140.143.132.167/vue/3.jpg "http://140.143.132.167/vue/3.jpg")[![http://140.143.132.167/vue/4.jpg](http://140.143.132.167/vue/4.jpg "http://140.143.132.167/vue/4.jpg")](http://140.143.132.167/vue/4.jpg "http://140.143.132.167/vue/4.jpg")[![http://140.143.132.167/vue/5.jpg](http://140.143.132.167/vue/5.jpg "http://140.143.132.167/vue/5.jpg")](http://140.143.132.167/vue/5.jpg "http://140.143.132.167/vue/5.jpg")