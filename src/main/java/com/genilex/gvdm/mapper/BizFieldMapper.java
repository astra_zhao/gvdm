package com.genilex.gvdm.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.genilex.gvdm.entity.BizField;
import com.genilex.gvdm.entity.BizFieldDic;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 */
public interface BizFieldMapper extends BaseMapper<BizField> {
}