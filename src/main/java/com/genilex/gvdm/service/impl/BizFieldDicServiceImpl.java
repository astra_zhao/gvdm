package com.genilex.gvdm.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.genilex.gvdm.entity.BizField;
import com.genilex.gvdm.entity.BizFieldDic;
import com.genilex.gvdm.mapper.BizFieldDicMapper;
import com.genilex.gvdm.service.IBizFieldDicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
public class BizFieldDicServiceImpl extends ServiceImpl<BizFieldDicMapper, BizFieldDic> implements IBizFieldDicService {
    @Autowired
    private BizFieldDicMapper bizFieldDicMapper;

    @Override
    public List<BizFieldDic> selectListData(Map<String, Object> map) {
        return bizFieldDicMapper.selectListData(map);
    }

    @Override
    public List<BizFieldDic> selectEnum(String name) {
        return bizFieldDicMapper.selectEnum(name);
    }
}
