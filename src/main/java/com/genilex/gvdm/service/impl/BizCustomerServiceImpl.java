package com.genilex.gvdm.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.genilex.gvdm.entity.BizCustomer;
import com.genilex.gvdm.mapper.BizCustomerMapper;
import com.genilex.gvdm.service.IBizCustomerService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class BizCustomerServiceImpl extends ServiceImpl<BizCustomerMapper, BizCustomer> implements IBizCustomerService {

}
