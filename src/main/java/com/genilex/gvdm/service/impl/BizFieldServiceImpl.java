package com.genilex.gvdm.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.genilex.gvdm.entity.BizCustomer;
import com.genilex.gvdm.entity.BizField;
import com.genilex.gvdm.entity.BizFieldDic;
import com.genilex.gvdm.mapper.BizCustomerMapper;
import com.genilex.gvdm.mapper.BizFieldMapper;
import com.genilex.gvdm.service.IBizFieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BizFieldServiceImpl extends ServiceImpl<BizFieldMapper, BizField> implements IBizFieldService {

}
