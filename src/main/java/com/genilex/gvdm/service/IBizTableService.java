package com.genilex.gvdm.service;

import com.baomidou.mybatisplus.service.IService;
import com.genilex.gvdm.entity.BizCustomer;
import com.genilex.gvdm.entity.BizTable;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2017-08-19
 */
public interface IBizTableService extends IService<BizTable> {
    List<BizTable> selectListData(Map<String,Object> map);
}
