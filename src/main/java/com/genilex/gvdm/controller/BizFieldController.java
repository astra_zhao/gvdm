package com.genilex.gvdm.controller;


import com.genilex.gvdm.core.ExecuteDTO;
import com.genilex.gvdm.core.PageQueryParamDTO;
import com.genilex.gvdm.core.PageResultDTO;
import com.genilex.gvdm.entity.BizField;
import com.genilex.gvdm.entity.BizFieldDic;
import com.genilex.gvdm.service.IBizFieldDicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/api/field")
// 精友字段维护
public class BizFieldController {

    @Autowired
    private IBizFieldDicService fieldDicService;

    @RequestMapping("/getPage")
    public PageResultDTO getTableDicPage(@RequestBody PageQueryParamDTO params) {
        Map<String,Object> map = new HashMap<>();
        if (params.getQuery()!=null){
            map.put("name", params.getQuery().toString());
        }
        map.put("page", params.getPage());
        map.put("size", params.getSize());

        List<BizFieldDic> list = fieldDicService.selectListData(map);

        PageResultDTO pageResultDTO = new PageResultDTO();
        pageResultDTO.setRows(list);
        pageResultDTO.setTotal(100);
        return pageResultDTO;
    }


    @RequestMapping("/save")
    public ExecuteDTO insertOrUpdateCustomer(@RequestBody BizFieldDic bizFieldDic){
        if (bizFieldDic.getId()==null){
            fieldDicService.insert(bizFieldDic);
            return new ExecuteDTO(true, "保存成功", bizFieldDic.getId());
        } else {
            fieldDicService.updateById(bizFieldDic);
            return new ExecuteDTO(true, "保存成功", bizFieldDic.getId());
        }
    }

}
